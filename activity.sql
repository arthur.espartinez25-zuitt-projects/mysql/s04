--a.
SELECT *
FROM artists
WHERE name LIKE "%d%";
--b.
SELECT * 
FROM songs
WHERE length > 230;
--c
SELECT albums.name, songs.title
FROM albums JOIN songs
ON albums.id=songs.album_id;
--d
SELECT artists.name AS artist_name, albums.name AS album_name
FROM artists JOIN albums
ON artists.id=albums.artist_id
WHERE albums.name LIKE "%a%";
--e
SELECT *
FROM albums
ORDER BY name DESC
LIMIT 4;
--f
SELECT *
FROM albums 
JOIN songs ON albums.id=songs.album_id
ORDER BY albums.name DESC, songs.title ASC;